# Profile Page
This is a skill's test project for a front-end position.
**Online Version:** http://filipeamaral.me/profile-front/
# Dependencies
  * Babel - Babel is a JavaScript compiler. Use next generation JavaScript, today.
  * Gulp - gulp is a toolkit for automating painful or time-consuming tasks in your development workflow.
  * Webpack - webpack is a module bundler. Its main purpose is to bundle JavaScript files for usage in a browser.
# How to run it
* First ensure you have [nodejs](https://nodejs.org/en/) 6+ and [npm](https://nodejs.org/en/) 5+ globally installed in your computer.
* Now, clone the project somewhere in your computer
```bash
$ git clone https://itisfilipe@bitbucket.org/itisfilipe/filipe_amaral.git
```
* Install the dependencies for the projetct:
```bash
$ cd filipe_amaral
$ npm install
```
* Now, you have to transpile the javascript and put everything togheter using webpack.
```
$ npm run build
```
* everything will be inside of the new **dist** folder.
