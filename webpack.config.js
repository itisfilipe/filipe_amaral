var path = require('path')
var CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
  entry: ["babel-polyfill", './app/js/App.js'],
  output: {
    path: path.resolve('./app/dist'),
    filename: 'bundle.js',
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: [
            [
              'env',
              {
                'targets': {
                  'browsers': [
                    'Chrome >= 52',
                    'FireFox >= 44',
                    'Safari >= 7',
                    'Explorer 11',
                    'last 4 Edge versions',
                  ],
                },
                'useBuiltIns': true
              },
            ]],
        },
      }],
  },
  devServer: {
    inline: true,
    host: '0.0.0.0',
    disableHostCheck: true,
    contentBase: './app',
    port: 3333,
    lazy: false,
  },
  plugins: [
    new CopyWebpackPlugin([
      {from: './app/index.html', to: './index.html'},
      {from: './app/vendor', to: './vendor'},
      {from: './app/imgs', to: './imgs'},
      {from: './app/css', to: './css'},
    ])],
}
