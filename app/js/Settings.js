import Mediator from './Mediator';
export default class Settings {
  /**
   * Page to be rendered by menu. Leave blank to match mockups
   */
  constructor() {
    Mediator.subscribe('settings', this.render.bind(this));
  }

  /**
   * returns the template, may be customized or not.
   * @returns {string}
   */
  template() {
    return `<h2 class="page__title">Settings <i class="icon ion-edit page__edit-icon page__edit-icon--hide-md" data-edit-popup="mobile"></i></h2>
        `;
  }

  /**
   * render the template inside index.html
   */
  render() {
    document.getElementById('page').innerHTML = this.template();
  }
}
