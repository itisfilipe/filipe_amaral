import Mediator from './Mediator';

export default class Header {
  /**
   * Dumb Component that render all children from header
   */
  constructor() {
    this.children = ['navigation', 'uploadCover', 'userDetails', 'menu'];
    Mediator.subscribe('header', this.render.bind(this));
  }

  /**
   * render the children specified using the event emitter
   */
  renderChildren() {
    for (const child of this.children) {
      Mediator.emit(child);
    }
  }

  /**
   * returns the template, may be customized or not.
   * @returns {string}
   */
  template() {
    return `
        <div id="navigation" class="navigation"></div>
        <div id="uploadCover" class="upload-cover"></div>
        <div class="user-details" id="userDetails"></div>
        <hr class="divider">
        <div class="menu" id="menu"></div>`;
  }

  /**
   * render the template inside index.html
   */
  render() {
    document.getElementById('header').innerHTML = this.template();
    this.renderChildren();
  }
}
