import Mediator from './Mediator';

export default class Menu {
  /**
   * Smart component that generates the menu and choose the right page to render
   * @param pages - menus page's/item's
   */
  constructor(pages) {
    this.children = ['followers'];
    this.firstLoad = true;
    Mediator.subscribe('menu', this.render.bind(this));
    this.pages = pages;
    this.selectedPage = 0;
  }

  /**
   * Bind mouse events to do some action when user click on menu
   */
  bindEvents() {
    const menuItems = document.querySelectorAll('[data-menu-key]');
    for (const item of menuItems) {
      item.addEventListener('click', this.renderPage.bind(this));
    }
  }

  /**
   * load menu and pages defined to show at beginning
   */
  loadDefaults() {
    const pageElem = this.pages[this.selectedPage].elem;
    pageElem.render();
  }

  /**
   * Render a page (About, Settings, etc)
   * @param e - mouse event from click on menu
   */
  renderPage(e) {
    // different apis iexplorer and chrome
    this.selectedPage = isNaN(parseInt(e.target.attributes['0'].value))
      ? parseInt(e.target.attributes['1'].value)
      : parseInt(e.target.attributes['0'].value);
    this.changeActiveItem();
    const pageElem = this.pages[this.selectedPage].elem;
    if (pageElem) {
      pageElem.render();
    } else {
      document.getElementById('page').innerHTML = '';
    }
  }

  /**
   * render the children specified using the event emitter
   */
  renderChildren() {
    for (const child of this.children) {
      Mediator.emit(child);
    }
  }

  /**
   * change the flag on menu to indicate what is the current menu item
   */
  changeActiveItem() {
    const menuItems = document.querySelectorAll('[data-menu-key]');

    for (let i = 0; i < menuItems.length; i++) {
      menuItems[i].className = 'menu__item';
      if (+this.selectedPage === i) {
        menuItems[i].className += ' menu__item--active';
      }
    }
  }

  /**
   * returns the template, may be customized or not.
   * @returns {string}
   */
  template() {
    const items = this.pages.map((item, id) => {
      const { name } = item;
      const cls = this.selectedPage === id ? 'menu__item--active' : '';
      return `<li data-menu-key="${id}" class="menu__item ${cls}">${name}</li>`;
    });
    return `<div class="menu__wrapper">
              <ul class="menu__row">
                  ${items.join('')}
              </ul>
          </div>
          <div id="followers" class="followers"></div>`;
  }

  /**
   * render the template inside index.html
   */
  render() {
    document.getElementById('menu').innerHTML = this.template();
    if (this.firstLoad) {
      this.firstLoad = false;
      this.loadDefaults();
      this.bindEvents();
    }
    this.renderChildren();
  }
}
