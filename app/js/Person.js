import Mediator from './Mediator';

// Example data
const personModel = {
  firstName: 'Jessica',
  lastName: 'Parker',
  address: 'Newport Beach, CA',
  telephone: '(949) 325 - 68594',
  website: 'www.seller.com'
};

export default class Person {
  /**
   * Manages user data
   */
  constructor() {
    this.loadUserData(personModel);
    Mediator.subscribe('userUpdate', this.updateElement.bind(this));
    Mediator.subscribe('userLoad', this.loadUserData.bind(this, personModel));
  }

  /**
   * update element in the page automatically
   * @param identifier - identifier of element eg data-person="identifier"
   * @param value
   */
  updateElement({ identifier, value }) {
    const elements = document.querySelectorAll(`[data-person="${identifier}"]`);
    if (!elements || value === '') return;
    for (const element of elements) {
      element.textContent = value;
    }
  }

  /**
   * load user data from object
   * @param data
   */
  loadUserData(data) {
    for (const key of Object.keys(data)) {
      this.updateElement({ identifier: key, value: data[key] });
    }
  }
}
