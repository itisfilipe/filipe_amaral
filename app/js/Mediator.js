class Mediator {
  /**
   * Encapsulates object interactions
   */
  constructor() {
    this.events = {};
  }

  /**
   * subscribe and listen for events
   * @param en - Event Name
   * @param cb - callback function to be called
   */
  subscribe(en, cb) {
    this.events[en] = this.events[en] || [];
    this.events[en].push(cb);
  }

  /**
   * Destroy some subscription
   * @param en - Event Name
   * @param cb - callback function to be called
   */
  destroy(en, cb) {
    if (this.events[en]) {
      this.events[en].filter(fn => fn !== cb);
    }
  }

  /**
   * Send/emit one event to all subscribers
   * @param en - Event Name
   * @param data - data sent to subscriber
   */
  emit(en, data) {
    if (this.events[en]) {
      for (const cb of this.events[en]) {
        cb(data);
      }
    }
  }
}

const mediator = new Mediator();
export default mediator;
