import Mediator from './Mediator';

export default class UploadCover {
  /**
   * dumb component for render a button
   */
  constructor() {
    Mediator.subscribe('uploadCover', this.render.bind(this));
  }

  /**
   * returns the template, may be customized or not.
   * @returns {string}
   */
  template() {
    return `<button class="button button--light">
            <i class="icon ion-camera upload-cover__icon"></i>
            <span class="upload-cover__button-text">Upload Cover Image</span>
        </button>`;
  }

  /**
   * render the template inside index.html
   */
  render() {
    document.getElementById('uploadCover').innerHTML = this.template();
  }
}
