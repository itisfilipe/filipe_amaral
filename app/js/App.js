import 'babel-polyfill';
import Mediator from './Mediator';
import Avatar from './Avatar';
import Followers from './Followers';
import Header from './Header';
import Menu from './Menu';
import Navigation from './Navigation';
import Ratings from './Ratings';
import UploadCover from './UploadCover';
import UserDetails from './UserDetails';
import Person from './Person';
import About from './About';
import Settings from './Settings';

class App {
  /**
   * Main App. Loads all the views and start the rendering process
   */
  constructor() {
    this.children = ['header'];
    this.loadDeps();
  }

  /**
   * Load all dependencies for rendering
   */
  loadDeps() {
    new Avatar();
    new Followers();
    new Header();
    const about = new About();
    const settings = new Settings();
    // provide the menu with his components to be rendered
    new Menu([
      { id: 1, name: 'ABOUT', elem: about },
      { id: 2, name: 'SETTINGS', elem: settings },
      { id: 3, name: 'OPTION1', elem: undefined },
      { id: 4, name: 'OPTION2', elem: undefined },
      { id: 5, name: 'OPTION3', elem: undefined }
    ]);
    new Navigation();
    new Ratings();
    new UploadCover();
    new UserDetails();
    new Person();
  }

  /**
   * render the children specified using the event emitter
   */
  renderChildren() {
    for (const child of this.children) {
      Mediator.emit(child);
    }
  }

  /**
   * returns the template, may be customized or not.
   * @returns {string}
   */
  template() {
    return `<div id="container" class="container">
              <div id="header" class="header"></div>
              <div class="page" id="page"></div>
            </div>`;
  }

  /**
   * render the template inside index.html
   */
  render() {
    document.getElementById('root').innerHTML = this.template();
    this.renderChildren();
  }
}

//start the app =)
const app = new App();
app.render();
