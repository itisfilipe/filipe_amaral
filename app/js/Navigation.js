import Mediator from './Mediator';

export default class Navigation {
  /**
   * Dumb component that renders top navigation
   */
  constructor() {
    Mediator.subscribe('navigation', this.render.bind(this));
  }

  /**
   * returns the template, may be customized or not.
   * @returns {string}
   */
  template() {
    return `<button class="button navigation__button-sm">LOG OUT</button>`;
  }

  /**
   * render the template inside index.html
   */
  render() {
    document.getElementById('navigation').innerHTML = this.template();
  }
}
