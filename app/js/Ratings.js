import Mediator from './Mediator';

export default class Ratings {
  /**
   * Dumb component to render rating and reviews
   */
  constructor() {
    Mediator.subscribe('ratings', this.render.bind(this));
  }

  /**
   * returns the template, may be customized or not.
   * @returns {string}
   */
  template() {
    return `<div class="ratings__stars">
                  <i class="icon ion-ios-star ratings__star"></i>
                  <i class="icon ion-ios-star ratings__star"></i>
                  <i class="icon ion-ios-star ratings__star"></i>
                  <i class="icon ion-ios-star ratings__star"></i>
                  <i class="icon ion-ios-star-outline ratings__star"></i>
              </div>
              <div class="ratings__reviews">6 <span class="ratings__reviews-text"> Reviews</span></div>
          </div>`;
  }

  /**
   * render the template inside index.html
   */
  render() {
    document.getElementById('ratings').innerHTML = this.template();
  }
}
