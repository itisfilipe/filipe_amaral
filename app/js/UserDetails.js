import Mediator from './Mediator';

export default class UserDetails {
  constructor() {
    this.children = ['pictureFrame', 'ratings'];
    Mediator.subscribe('userDetails', this.render.bind(this));
  }

  /**
   * render the children specified using the event emitter
   */
  renderChildren() {
    for (const child of this.children) {
      Mediator.emit(child);
    }
  }

  /**
   * returns the template, may be customized or not.
   * @returns {string}
   */
  template() {
    return `<div id="pictureFrame" class="picture-frame"></div>
            <div class="personal-info">
                <div class="contact">
                    <div class="contact__name"><span data-person="firstName"></span> <span data-person="lastName"></span></div>
                    <div class="contact__details">
                        <div class="contact__street">
                            <i class="icon ion-ios-location-outline contact__street-icon"></i>
                            <span data-person="address"></span>
                        </div>
                        <div class="contact__telephone">
                            <i class="icon ion-ios-telephone-outline contact__telephone-icon"></i>
                            <span data-person="telephone"></span>
                        </div>
                    </div>
                </div>
                <div id="ratings" class="ratings"></div>
            </div>`;
  }

  /**
   * render the template inside index.html
   */
  render() {
    document.getElementById('userDetails').innerHTML = this.template();
    this.renderChildren();
  }
}
