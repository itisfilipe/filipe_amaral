import Mediator from './Mediator';
import Popup from './Popup';

export default class About {
  /**
   * Dumb Component. About page to be rendered by menu
   */
  constructor() {
    Mediator.subscribe('about', this.render.bind(this));
  }

  /**
   * returns the template, may be customized or not.
   * @returns {string}
   */
  template() {
    return `<div id="about"><h2 class="page__title">About <i class="icon ion-edit page__edit-icon page__edit-icon--hide-md" data-edit-popup="mobile"></i></h2>
        <ul class="editable-fields">
            <li class="editable-fields__item editable-fields__name"><span data-person="firstName"></span> <span data-person="lastName"></span><i class="icon ion-edit page__edit-icon page__edit-icon--hide-sm" data-edit-popup="name"></i></li>
            <li class="editable-fields__item">
                <i class="icon ion-android-globe editable-fields__icon-left"></i><span data-person="website"></span>
                <i class="icon ion-edit page__edit-icon page__edit-icon page__edit-icon--hide-sm"
                   data-edit-popup="website"></i></li>
            <li class="editable-fields__item"><i
                    class="icon ion-ios-telephone-outline editable-fields__icon-left"></i><span data-person="telephone"></span><i class="icon ion-edit page__edit-icon page__edit-icon--hide-sm"
                                     data-edit-popup="telephone"></i></li>
            <li class="editable-fields__item"><i class="icon ion-ios-home-outline editable-fields__icon-left"></i><span
                    data-person="address"></span><i class="icon ion-edit page__edit-icon page__edit-icon--hide-sm"
                             data-edit-popup="address"></i></li>
        </ul></div>`;
  }

  /**
   * render the template inside index.html
   */
  render() {
    document.getElementById('page').innerHTML = this.template();
    Mediator.emit('userLoad');
    // wee need to create a new object everytime we re-render in
    // order to make binds work
    new Popup('[data-edit-popup]');
  }
}
