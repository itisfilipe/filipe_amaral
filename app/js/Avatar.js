import Mediator from './Mediator';

export default class Avatar {
  /**
   * Render the profile picture on header
   */
  constructor() {
    Mediator.subscribe('pictureFrame', this.render.bind(this));
  }

  /**
   * returns the template, may be customized or not.
   * @returns {string}
   */
  template() {
    return `<img class="picture-frame__image" src="./imgs/profile_image.jpg" alt="">`;
  }

  /**
   * render the template inside index.html
   */
  render() {
    document.getElementById('pictureFrame').innerHTML = this.template();
  }
}
