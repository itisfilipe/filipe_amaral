import Mediator from './Mediator';

export default class Followers {
  /**
   * Dumb component to rende the number of values
   */
  constructor() {
    Mediator.subscribe('followers', this.render.bind(this));
  }

  /**
   * returns the template, may be customized or not.
   * @returns {string}
   */
  template() {
    return `<i class="icon ion-ios-plus followers__icon"></i>
              15 <span class="followers__text">Followers</span>`;
  }

  /**
   * render the template inside index.html
   */
  render() {
    document.getElementById('followers').innerHTML = this.template();
  }
}
