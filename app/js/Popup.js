import Mediator from './Mediator';

// Constants to check the type of component
const NAME = 'NAME';
const WEBSITE = 'WEBSITE';
const TELEPHONE = 'TELEPHONE';
const ADDRESS = 'ADDRESS';

// data to be used when generating forms
const formMeta = {
  name: [
    {
      label: 'FIRST NAME',
      id: 'firstName'
    },
    {
      label: 'LAST NAME',
      id: 'lastName'
    }
  ],
  website: {
    label: 'WEBSITE',
    id: 'website'
  },
  telephone: {
    label: 'PHONE NUMBER',
    id: 'telephone'
  },
  address: {
    label: 'CITY, STATE & ZIP',
    id: 'address'
  }
};

export default class Popup {
  /**
   * Create a edit Popup for profile page.
   * @param {string} selector - selector used to hook click events
   */
  constructor(selector) {
    this.popupHook = this.addHookToBody();
    this.popupButtons = document.querySelectorAll(selector);
    this.isOpen = false;
    this.bindEvents();
  }

  /**
   * bind click events for open and close popup
   */
  bindEvents() {
    // add a handler to all element tagged with data-edit-popup
    for (const button of this.popupButtons) {
      button.addEventListener('click', this.open.bind(this));
    }
    // close the pop up if user click somewhere
    document.body.addEventListener('click', this.close.bind(this));
    // this is to avoid the popup to close if user click inside of it
    this.popupHook.addEventListener('click', e => e.stopPropagation());
  }

  /**
   *
   * @param {int} x - position x where popup should appear
   * @param {int} y - position y where popup should appear
   * @param {string} fieldRef - indicate for wich data we will render
   */
  render(x, y, fieldRef) {
    const isMobile = window.innerWidth < 769;
    if (!isMobile) {
      this.popupHook.style.top = y - 20 + 'px';
      this.popupHook.style.left = x + 30 + 'px';
    } else {
      this.popupHook.style.position = 'relative';
    }
    const popup = this.createPopup(fieldRef);
    this.killOpenedPopups();
    this.popupHook.appendChild(popup);
    this.bindPopupElements();
  }

  /**
   *
   * @param {string} fieldRef - indicate for wich data we will render
   * @returns {Object | Array} - data relevant to generate the field
   */
  popupDataFromReference(fieldRef) {
    switch (fieldRef.toUpperCase()) {
      case NAME:
        return formMeta.name;
      case WEBSITE:
        return formMeta.website;
      case TELEPHONE:
        return formMeta.telephone;
      case ADDRESS:
        return formMeta.address;
      default:
        return Object.values(formMeta);
    }
  }

  /**
   * bind elements inside the pop up
   */
  bindPopupElements() {
    const form = document.getElementById('popupForm');
    form.addEventListener('submit', this.submitForm.bind(this, form));
    document
      .getElementById('popupCloseButton')
      .addEventListener('click', this.close.bind(this));
  }

  /**
   * handler for form submission
   * @param {MouseEvent} e
   */
  submitForm(e) {
    const form = arguments[0];
    const event = arguments[1];
    event.preventDefault();
    const inputs = Array.from(form.querySelectorAll('input'));
    for (const input of inputs) {
      const { id, value } = input;
      Mediator.emit('userUpdate', { identifier: id, value });
    }
    this.killOpenedPopups();
  }

  /**
   * handler to open a popup
   * @param {MouseEvent} event
   */
  open(event) {
    event.preventDefault();
    const fieldRef = event.target.attributes[1].value;
    // workaround to avoid it to close immediately after open
    setTimeout(() => {
      this.render(event.pageX, event.pageY, fieldRef);
      this.isOpen = true;
    }, 100);
  }

  /**
   * handler to close a popup
   * @param {MouseEvent} event
   */
  close(event) {
    event.preventDefault();
    if (this.isOpen) {
      this.killOpenedPopups();
    }
  }

  /**
   * add hook into body for popup be inserted later
   * @returns {Element} - DOM element
   */
  addHookToBody() {
    const div = document.createElement('div');
    div.id = 'popupHook';
    div.className = 'popup-hook';
    const body = document.body;
    body.appendChild(div);
    return div;
  }

  /**
   * generate the inpun template dynamically
   * @param {Object} data - information for input generation
   * @returns {string} - input template
   */
  generateInputs(data) {
    if (Array.isArray(data)) {
      return data.reduce((acc, input) => {
        // we could have arrays inside of arrays
        if (Array.isArray(input)) {
          return acc + this.generateInputs(input);
        } else {
          return (
            acc +
            `<div class="edit-popup__form">
              <input class="edit-popup__input" id="${input.id}" type="text" required="required"/>
              <span class="edit-popup__highlight"></span>
              <span class="edit-popup__bar"></span>
              <label class="edit-popup__label" for="input">${input.label}</label><i class="bar"></i>
            </div>`
          );
        }
      }, '');
    } else {
      return `<div class="edit-popup__form">
              <input class="edit-popup__input" id="${data.id}" type="text" required="required"/>
              <span class="edit-popup__highlight"></span>
              <span class="edit-popup__bar"></span>
              <label class="edit-popup__label" for="input">${data.label}</label><i class="bar"></i>
            </div>`;
    }
  }

  /**
   * generate the poopup template
   * @param {Object} data - information for input generation
   * @returns {string} - the html template
   */
  template(data) {
    const inputs = this.generateInputs(data);
    return `<div class="edit-popup">
            <form class="edit-popup__form" id="popupForm">
              <div>
                ${inputs}
              </div>
              <div class="edit-popup__buttons">
                <button class="button button--primary edit-popup__button" type="submit"><span>SAVE</span></button>
                <button class="button edit-popup__button" type="button" id="popupCloseButton"><span>CANCEL</span></button>
              </div>
          </form>
       </div>`;
  }

  /**
   * create the pop up element
   * @param {string} fieldRef - indicate for wich data we will render
   * @returns {Element} popup DOM element
   */
  createPopup(fieldRef) {
    const formData = this.popupDataFromReference(fieldRef);
    const div = document.createElement('div');
    div.className = 'popup-wrapper';
    div.innerHTML = this.template(formData);
    return div;
  }

  /**
   * kill any opened popup. it must be only one opened
   */
  killOpenedPopups() {
    // if not defined use the default hook
    while (this.popupHook.firstChild) {
      this.popupHook.removeChild(this.popupHook.firstChild);
    }
    this.isOpen = false;
  }
}
