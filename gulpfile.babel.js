import gulp from 'gulp'
import sass from 'gulp-sass'
import autoprefixer from 'gulp-autoprefixer'
import sourcemaps from 'gulp-sourcemaps'
import gutil from "gulp-util"

// transpile styles
gulp.task('sass', () => {
  gulp.src('app/sass/*.scss').
    pipe(sourcemaps.init()).
    pipe(sass.sync().on('error', () => new gutil.PluginError('sass', sass.logError))).
    pipe(autoprefixer()).
    pipe(sourcemaps.write('.')).
    pipe(gulp.dest('./app/css/'))
})

const autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};

gulp.task('prod',  () => {
  return gulp
  .src('app/sass/*.scss')
  .pipe(sass({ outputStyle: 'compressed' }))
  .pipe(autoprefixer(autoprefixerOptions))
  .pipe(gulp.dest('./app/css/'));
});

//Watch task
gulp.task('watch', function() {
  gulp.watch('./app/sass/**/**/*.scss', ['sass']);
});

gulp.task('default', ['sass', 'watch']);
